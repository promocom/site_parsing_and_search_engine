package net.freehaven.tor.control;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Eugene Chyrski
 * @since 3/17/2015
 */
public class NewNym {
    private static TorControlConnection connection;

    public static void
    doNewNym() throws IOException {
        synchronized (NewNym.class) {
            try {
                if(connection==null) {
                    connection = new TorControlConnection(new Socket("127.0.0.1", 9051));
                    connection.launchThread(false);
                    connection.authenticate("newnym".getBytes());
                }
                connection.signal("NEWNYM");

            } catch (Exception err) {

            }
        }

    }
}
