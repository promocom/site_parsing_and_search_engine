import my.cen.Wiki2;
import my.cen.config.MainConfig;
import my.cen.index.StringIndex;
import my.cen.service.ElasticSearchService;
import my.cen.utils.DocumentUtils;
import my.cen.utils.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.log4j.Logger;
import org.elasticsearch.common.collect.ImmutableBiMap;
import org.elasticsearch.common.collect.ImmutableMap;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created  on 10/21/2015.
 */
public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);

    public static final void main(String... args) throws Exception {
        LineIterator iterator = FileUtils.lineIterator(new File("/home/???/Projects/parsed.txt"));
        logger.info("starting");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
        context.start();
        ElasticSearchService service = (ElasticSearchService) context.getBean("elasticSearchService");
        boolean b=true;
        while(b){
            Thread.sleep(1000);
        }
        while (iterator.hasNext()) {
            String line = iterator.next();
            if (line.endsWith("|"))
                line = line.substring(0, line.length() - 1);
            String terms[] = line.split(";", 2);
            String keyword = terms[terms.length == 2 ? 1 : 0];
            final String tag = DocumentUtils.prepareTag(keyword);
            service.processQueryResults(new String[]{}, QueryBuilders.termQuery("tags", tag), new ElasticSearchService.ResultProcessor() {
                @Override
                public void process(SearchHit hit, long totalCount, int currentNumber) {
                    if (currentNumber == totalCount)
                        System.out.println("Found " + tag + " " + totalCount + " times");
                }
            });
        }
    }
}
