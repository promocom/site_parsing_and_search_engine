package my.cen.utils;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;

/**
 * Created by PC on 15.12.2014.
 */
public class StringUtils {


    public static String getDocumentType(URL url) {
        return url.getHost().replace("www.", "");
    }

    public static String getRelativePart(String link) {
        try {
            URL url = new URL(link);
            return url.getPath() + (url.getQuery() == null ? "" : "?" + url.getQuery());
        } catch (MalformedURLException e) {

        }
        return link;
    }

    public static String encode(String in) {
        try {
            MessageDigest sha = MessageDigest.getInstance("MD5");

            return new BigInteger(1, sha.digest(in.getBytes())).toString(Character.MAX_RADIX);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
