package my.cen;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.code.geocoder.AdvancedGeoCoder;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.*;
import my.cen.index.StringIndex;
import my.cen.utils.ThreadUtils;
import net.freehaven.tor.control.NewNym;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.collect.ImmutableList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created  on 9/6/2015.
 */
public class Geo {
    public static void main(String[] args) throws Exception {
        StringIndex index = new StringIndex(new File("/geo.index"));
        StringIndex newIndex = new StringIndex(new File("/geo_new.index"));

        index.open();
        newIndex.open();
        //  FileUtils.readLines(new File("/geo_new.txt")).forEach(s -> newIndex.searchOrIndex(s.toLowerCase()));
        //  newIndex.flush();
        boolean nextPass = true;
        while (nextPass) {
            nextPass = false;
            Collection<String> lines = new TreeSet<>();
            lines.addAll(FileUtils.readLines(new File("/geo_new.index")));
            lines.addAll(FileUtils.readLines(new File("i:\\code\\src\\main\\cities.txt")));

            Map content = new ObjectMapper().readValue((InputStream) new URL("http://portal.mail.ru/RegionSuggest").getContent(), Map.class);
            for (Map regionInfo : (Collection<Map>) ((Map) content.get("regions")).values()) {
                String city = String.valueOf(regionInfo.get("cityName")).trim();
                String region = String.valueOf(regionInfo.get("regionName")).trim();
                if ("".equals(city)) {
                    lines.add(region);
                } else if ("".equals(region)) {

                    lines.add(city);
                } else {
                    lines.add(region);
                    lines.add(StringUtils.join(new String[]{city, region}, ";"));
                }
            }
            lines.remove("");

            System.setProperty("socksProxyHost", "127.0.0.1");
            System.setProperty("socksProxyPort", "9050");
            BufferedWriter writer = new BufferedWriter(new FileWriter("/cities_" + System.currentTimeMillis() + ".geo"));
            HttpClient client = new HttpClient(new MultiThreadedHttpConnectionManager());
            client.setTimeout((int) TimeUnit.SECONDS.toMillis(5));
            Geocoder geocoder = new AdvancedGeoCoder(client);
            Collection<String> lang = ImmutableList.of("en", "ru");
            int i = 1;
            for (String line : lines) {

                String geo = line.replace(";", " , ");
                if (!index.searchOrIndex(geo)) {
                    if (geo.startsWith("#")) {

                        continue;

                    }
                    for (String lng : lang) {
                        boolean found = false;
                        while (!found) {
                            try {


                                nextPass = true;
                                NewNym.doNewNym();
                                GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setLanguage(lng).setAddress(geo).getGeocoderRequest();
                                GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
                                if (geocoderResponse.getStatus() == GeocoderStatus.OVER_QUERY_LIMIT) {
                                    NewNym.doNewNym();
                                    ThreadUtils.sleepNoInterrupt(TimeUnit.SECONDS.toMillis(5));
                                } else {
                                    found = true;

                                    if (geocoderResponse.getStatus() == GeocoderStatus.OK) {
                                        System.out.println(geocoderResponse.getResults());
                                        if (geocoderResponse.getResults().size() == 1) {
                                            GeocoderResult res = geocoderResponse.getResults().get(0);
                                            String types = StringUtils.join(res.getTypes(), "|");
                                            LatLng location = res.getGeometry().getLocation();
                                            if (!line.contains(";")) {
                                                line = line + ";" + res.getAddressComponents().get(res.getAddressComponents().size() - 1).getLongName();
                                            }
                                            if (!res.getTypes().contains("route") && !res.getTypes().contains("postal_code")) {
                                                writer.write(line + ";" + location.getLat() + ";" + location.getLng() + ";" + types);
                                                writer.newLine();
                                                writer.flush();
                                            }

                                        }
                                        for (GeocoderResult res : geocoderResponse.getResults()) {
                                            for (GeocoderAddressComponent comp : res.getAddressComponents()) {
                                                if (!isNumeric(comp.getLongName()))
                                                    newIndex.searchOrIndex(comp.getLongName().replace("\n", "").replace("\r", "").toLowerCase());
                                                if (!isNumeric(comp.getShortName()))
                                                    newIndex.searchOrIndex(comp.getShortName().replace("\n", "").replace("\r", "").toLowerCase());
                                            }
                                        }
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                if (i++ % 100 == 0) {
                    index.flush();
                    newIndex.flush();
                }
            }

        }


        index.flush();
        newIndex.flush();
        index.close();
        newIndex.close();
        System.exit(0);
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
