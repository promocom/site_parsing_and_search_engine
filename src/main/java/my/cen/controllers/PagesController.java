package my.cen.controllers;

import my.cen.model.spider.SiteParsingOptions;
import my.cen.model.spider.SiteParsingOptionsHolder;
import my.cen.service.ElasticSearchService;
import my.cen.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by PC on 28/1/14.
 */
@Controller
public class PagesController {

    @Autowired
    private ElasticSearchService elasticSearchService;
    @Autowired
    private SiteParsingOptionsHolder holder;
    @Autowired
    private StatisticsService stat;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getUi(Model model, @RequestParam(required = false, value = "keywords") String keywords,
                        @RequestParam(required = false, value = "date") Date date) {

        model.addAttribute("sites", new ArrayList<>(holder.allOptions()).stream().map(new Function<SiteParsingOptions, String>() {
            @Override
            public String apply(SiteParsingOptions siteParsingOptions) {
                return siteParsingOptions.getSite().getHost();
            }
        }).collect(Collectors.toList()));
        model.addAttribute("stat", stat.getFullStatistics());
        String header = "";
        if (StringUtils.isEmpty(keywords)) {
            keywords = "*";
            header = "All latest news";
        } else {
            header = "Search results for \"" + keywords + "\"";
        }
        Map<String, Object> res = elasticSearchService.searchFor(keywords, 100, "date", null, date);

        res.put("exp", keywords);
        if (date != null) {
            res.put("dt", new SimpleDateFormat("MM/dd/yyyy").format(date));
        }
        model.addAttribute("searchRes", res);
        model.addAttribute("headerText", header);
        return "view";
    }


}