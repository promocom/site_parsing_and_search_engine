package my.cen.discovery;

import my.cen.index.StringIndex;

import static my.cen.utils.DocumentUtils.*;

import my.cen.utils.DocumentUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.common.util.concurrent.ThreadFactoryBuilder;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * 
 * @since 9/7/2015.
 */
final class SiteDiscoveryServiceImpl implements Closeable {
    private static final Logger logger = Logger.getLogger(SiteDiscoveryServiceImpl.class);
    private static final int HTTP_TIMEOUT = (int) TimeUnit.MINUTES.toMillis(2);

    private final StringIndex processedIndex;
    private final StringIndex failedIndex;
    private final ThreadPoolExecutor executor;
    private final BufferedWriter discoveryWriter;


    public SiteDiscoveryServiceImpl(File folder, int maxThreadSize) throws Exception {
        if (!folder.exists() || folder.isFile()) {
            folder.mkdirs();
        }
        File processedFile = new File(folder.getAbsolutePath() + "/processed.ind");
        File failedFile = new File(folder.getAbsolutePath() + "/failed.ind");
        if (!processedFile.exists()) processedFile.createNewFile();
        if (!failedFile.exists()) failedFile.createNewFile();
        processedIndex = new StringIndex(processedFile);
        processedIndex.open();
        failedIndex = new StringIndex(failedFile);
        failedIndex.open();

        executor = new ThreadPoolExecutor(maxThreadSize, maxThreadSize, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<>(200));
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("discovery-%d").build();
        executor.setThreadFactory(namedThreadFactory);
        discoveryWriter = new BufferedWriter(new FileWriter(folder.getAbsoluteFile() + "/discovered.txt", true));
    }

    public void processDomains(String domainsFile, long timeout) {
        logger.info("Processing file " + domainsFile);
        long start = System.currentTimeMillis();
        File domainFile = new File(domainsFile);
        if (!domainFile.exists()) {
            throw new IllegalArgumentException("File provided doesnot exists");
        }
        Iterator<String> domains = null;

        try {
            domains = FileUtils.lineIterator(domainFile);
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to read file provided " + domainFile.getAbsolutePath());
        }

        List<Callable<Void>> batch = new ArrayList<>();
        while (domains.hasNext()) {
            batch.add(createDomainProcessingThread(domains.next()));
            if (batch.size() % 1000 == 0) {
                flushBatch(batch);
                batch.clear();
            }
            if (System.currentTimeMillis() - start > timeout) {
                return;
            }

        }

        if (batch.size() > 0) {
            flushBatch(batch);
        }

    }


    private void flushBatch(Collection<Callable<Void>> callables) {
        try {
            executor.invokeAll(callables, 2, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            processedIndex.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            failedIndex.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Callable<Void> createDomainProcessingThread(final String domain) {

        return () -> {
            // logger.debug("Started processing domain " + domain);
            try {
                if (DocumentUtils.isParsableSite(domain)) {
                    synchronized (discoveryWriter) {
                        String lineFound = padRight(domain, 25);
                        discoveryWriter.write(lineFound);
                        discoveryWriter.newLine();
                        discoveryWriter.flush();
                        logger.info("Found parsable site: " + lineFound);
                    }
                }

            } catch (Exception e) {
                failedIndex.searchOrIndex(domain);
                // logger.error("Error processing domain "+domain,e);
            } finally {

                // logger.debug("Finished processing domain " + domain);
            }
            return null;
        };
    }

    @Override
    public void close() throws IOException {
        try {
            executor.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            if (discoveryWriter != null)
                discoveryWriter.close();
        } finally {
            try {
                if (failedIndex != null) {
                    try {
                        failedIndex.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    failedIndex.close();
                }
            } finally {
                if (processedIndex != null) {
                    try {
                        processedIndex.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    processedIndex.close();
                }
            }
        }
    }
}
