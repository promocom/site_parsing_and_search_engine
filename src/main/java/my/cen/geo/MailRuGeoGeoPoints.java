package my.cen.geo;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

/**
 * Created  on 9/26/2015.
 */
public class MailRuGeoGeoPoints {

    public static void main(String []args) throws Exception  {
        Map content = new ObjectMapper().readValue((InputStream) new URL("http://portal.mail.ru/RegionSuggest").getContent(), Map.class);
        for (Map regionInfo : (Collection<Map>) ((Map) content.get("regions")).values()) {
            String city = String.valueOf(regionInfo.get("cityName"));
            String region = String.valueOf(regionInfo.get("regionName"));
            System.out.println(city + " " + region);
        }
    }
}
