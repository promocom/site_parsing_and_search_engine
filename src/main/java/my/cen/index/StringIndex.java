package my.cen.index;

import org.apache.log4j.Logger;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * Problem definition:
 * External data processing system provides a string and needs to know whether or not this string has been provided prior.
 * <p>
 * Implementation:
 * 1. All strings provided are firstly stored in MEMORY
 * 2. This is up to external system when the in-memory data becomes persistent (by calling flush method)
 * 3. Data is stored in flat lexicographically ordered file
 * 4. Data is accessed via memory - mapped  buffers
 * <p>
 * TODO: rewrite search method if the current one is slow ???
 * TODO: keep track of index bounds and search only for those strings which might be inside these bounds.
 * TODO: create memory mapped windows dynamically for really big files(Might not be necessary for current use-case)
 *
 * @author Eugene Chyrski
 * @since 2015-09-04 03:21:44
 */
public class StringIndex implements Index {
    private static final Logger logger = Logger.getLogger(StringIndex.class);
    private final Object __MUTEX__ = new Object();

    private static final byte DEFAULT_PAGE_SIZE_KB = 4; //because i like this number
    private final int bytesPerPage;
    private final File file;
    private final SortedSet<String> cache;
    private final List<MappedByteBuffer> chunks;
    private FileChannel fileChannel;
    private long fileSize;


    /**
     * @param file index file
     */
    public StringIndex(File file) {
        this(file, DEFAULT_PAGE_SIZE_KB);
    }

    /**
     * @param file     index file
     * @param pageSize memory mapping page size in Kbytes
     */
    public StringIndex(File file, byte pageSize) {
        this.bytesPerPage = pageSize * 1024;
        this.file = file;
        this.cache = Collections.synchronizedSortedSet(new TreeSet<>());
        this.chunks = new ArrayList<>();

    }

    public boolean searchOrIndex(final String term) {

        if (term == null) return false;
        String stripped = term.replace("\r", "").replace("\n", "").replace("\t", "").trim();
        if (stripped.equals("")) return false;
        if (cache.contains(stripped)) {
            return true;
        }
        synchronized (__MUTEX__) {
            SearchResult res = search(stripped);
            if (res.found)
                return true;
        }


        index(stripped);
        return false;

    }

    public boolean searchTerm(final String term) {

        if (term == null) return false;
        String stripped = term.replace("\r", "").replace("\n", "").replace("\t", "").trim();
        if (stripped.equals("")) return false;
        if (cache.contains(stripped)) {
            return true;
        }
        synchronized (__MUTEX__) {
            SearchResult res = search(stripped);
            if (res.found)
                return true;
        }
        return false;

    }

    private void index(final String term) {
        cache.add(term);
    }

    public void open() throws IOException {
        close();
        if (!file.exists() || !file.isFile()) {
            throw new IllegalStateException("File provided either does not exists or is not a file");
        }

        FileInputStream randomAccessFile = new FileInputStream(file);
        this.fileChannel = randomAccessFile.getChannel();
        this.fileSize = fileChannel.size();
        long start = 0, length = 0;
        for (int index = 0; start + length < fileChannel.size(); index++) {
            if ((fileChannel.size() / bytesPerPage) == index)
                length = (fileChannel.size() - index * bytesPerPage);
            else
                length = bytesPerPage;
            start = index * bytesPerPage;
            chunks.add(index, fileChannel.map(FileChannel.MapMode.READ_ONLY, start, length));
        }

    }


    public void flush() throws Exception {
        logger.debug("Index flush started");
        try {
            if (cache.size() == 0)
                return;
            synchronized (__MUTEX__) {

                Set<String> localCache = new TreeSet<>(cache);
                cache.removeAll(localCache);
                Map<Long, Set<String>> termAddresses = new TreeMap<>();
                for (String term : localCache) {
                    SearchResult res = search(term);
                    if (res.found) {
                        logger.warn("Value " + term + " already exists in index");
                        continue;
                    }
                    StringBuilder localTerm = new StringBuilder(term.length() + 1);
                    boolean gt = res.text != null && res.text.compareTo(term) < 0;
                    if (gt) {
                        if (res.endIndex != fileSize || fileSize == 0) {
                            localTerm.append(term);
                        } else {
                            localTerm.append('\n');
                            localTerm.append(term);
                        }
                    } else {

                        localTerm.append(term);
                        if (fileSize > 0) {
                            localTerm.append("\n");
                        }
                    }
                    long address = gt ? res.endIndex : res.startIndex;
                    Set<String> terms = termAddresses.get(address);
                    if (terms == null) {
                        terms = new TreeSet<>();
                        termAddresses.put(address, terms);
                    }
                    terms.add(localTerm.toString());
                }
                File tempFile = new File(file.getAbsolutePath() + "~");
                if (tempFile.exists()) {
                    while (!tempFile.delete()) {
                        Thread.sleep(600);
                    }
                }
                RandomAccessFile r = new RandomAccessFile(file, "r");
                RandomAccessFile rtemp = new RandomAccessFile(tempFile, "rw");
                long fileSize = r.length();
                long initialOffset = 0;
                try (FileChannel sourceChannel = r.getChannel()) {
                    try (FileChannel targetChannel = rtemp.getChannel()) {
                        if (sourceChannel.size() == 0) {
                            System.out.println("sourceChannel.size() == 0");
                            // rtemp.write('\n');
                        }
                        Set<Map.Entry<Long, Set<String>>> entries = termAddresses.entrySet();
                        for (Map.Entry<Long, Set<String>> entry : entries) {
                            long offset = entry.getKey();

                            sourceChannel.position(0).transferTo(initialOffset, offset - initialOffset, targetChannel);
                            int lineCounter = 0;
                            for (String line : entry.getValue()) {
                                if (offset == 0 && lineCounter > 0 && !line.endsWith("\n")) {
                                    rtemp.write('\n');
                                }
                                rtemp.write(line.getBytes("UTF-8"));
                                lineCounter++;
                            }
                            initialOffset = offset;
                        }
                        sourceChannel.transferTo(initialOffset, fileSize - initialOffset, targetChannel);
                    }
                }

                // while block below is running we are not able to work with this index;

                close();
                while (!file.delete()) {
                    System.gc();
                    Thread.sleep(600);
                }
                while (!tempFile.renameTo(file)) {
                    Thread.sleep(600);
                }
                open();
            }
        } finally {
            logger.debug("Flush finished");
        }
    }


    private synchronized SearchResult search(String term) {
        SearchResult result = new SearchResult();
        if (fileSize == 0) {
            return result;
        }
        long startPositionLow = 0;
        long startPositionHigh = fileSize / 2;
        long endPositionLow = fileSize / 2;
        long endPositionHigh = fileSize - 1;
        boolean found = false;
        long prevPosition = 0;
        try {
            result = extractStringAt(endPositionLow);
            int compare = result.text.compareTo(term);
            found = compare == 0;
            while (!found) {
                if (compare < 0) {
                    startPositionLow = startPositionHigh;
                    startPositionHigh = startPositionLow + (endPositionHigh - startPositionLow) / 2;
                    endPositionLow = startPositionLow + (endPositionHigh - startPositionLow) / 2;
                    endPositionLow = Math.min(fileSize - 1, Math.max(endPositionLow, result.endIndex + 1));
                } else if (compare > 0) {
                    endPositionHigh = endPositionLow;
                    startPositionHigh = startPositionLow + (endPositionHigh - startPositionLow) / 2;
                    endPositionLow = startPositionLow + (endPositionHigh - startPositionLow) / 2;
                    endPositionLow = Math.min(fileSize - 1, Math.max(0, Math.min(endPositionLow, result.startIndex - 2)));
                }
                //    System.out.println("" + term + " startLow:" + startPositionLow + " endLow:" + endPositionLow + " startHigh:" + startPositionHigh + " endHigh: " + endPositionHigh);
                if (prevPosition == endPositionLow || startPositionLow > endPositionLow || startPositionLow == endPositionLow || startPositionHigh == endPositionHigh) {
                    break;
                }
                result = extractStringAt(endPositionLow);
                prevPosition = endPositionLow;
                compare = result.text.compareTo(term);
                found = compare == 0;

            }
        } catch (IndexOutOfBoundsException e) {
            logger.error("Error searching of string: " + term + " startLow:" + startPositionLow + " endLow:" + endPositionLow + " startHigh:" + startPositionHigh + " endHigh: " + endPositionHigh, e);

        }
        result.found = found;
        return result;

    }


    private SearchResult extractStringAt(long endPosition) {

        long position = endPosition;
        SearchResult res = new SearchResult();
        char candidate = getChar(position);
        while (position > 0 && '\n' != candidate) {
            position--;
            candidate = getChar(position);
        }
        if ('\n' == candidate) {
            position++;
        }
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream(1024)) {
            candidate = 0;
            res.startIndex = position;
            while (candidate != '\n' && position < fileSize) {
                candidate = getChar(position);
                if (candidate != '\n')
                    baos.write(candidate);

                position++;
            }
            res.endIndex = position;
            try {
                res.text = baos.toString("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void close() throws IOException {
        chunks.clear();
        System.gc();
        if (fileChannel != null && fileChannel.isOpen()) {
            fileChannel.close();
        }
    }

    private char getChar(long position) {
        int page = (int) (position / bytesPerPage);
        int index = (int) (position % bytesPerPage);
        return (char) chunks.get(page).get(index);

    }

    private static final class SearchResult {
        private String text;
        private long startIndex;
        private long endIndex;
        private boolean found;

        @Override
        public String toString() {
            return "SearchResult{" +
                    "text='" + text + '\'' +
                    ", startIndex=" + startIndex +
                    ", endIndex=" + endIndex +
                    ", found=" + found +
                    '}';
        }
    }

    public static void main(String[] args) throws Exception {


        try (StringIndex index = new StringIndex(new File("g:\\home\\parser\\dailytechinfo_org.ext"))) {
            index.open();
            index.searchOrIndex("top100.rambler.ru");

        }
    }
}
