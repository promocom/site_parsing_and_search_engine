package my.cen.index;

import java.io.Closeable;
import java.io.IOException;

/**
 * Defines contract for the index classes
 *
 * @author Eugene Chyrski
 * @since 2015-09-04 03:21:44
 */
public interface Index extends Closeable {

    boolean searchOrIndex(final String term);

    void open() throws IOException;

}
