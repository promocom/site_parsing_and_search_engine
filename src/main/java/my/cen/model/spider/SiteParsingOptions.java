package my.cen.model.spider;

import my.cen.parser.IParser;
import my.cen.utils.StringUtils;
import org.springframework.util.Assert;

import java.net.URL;
import java.util.List;

/**
 * Created by PC on 15.12.2014.
 */
public class SiteParsingOptions {
    private final URL site;
    private final List<String> exclusions;
    private final String siteKey;
    private final IParser parser;
    private final boolean hash;
    private final int threadCount;
    private final int pagesPerThread;

    public SiteParsingOptions(URL site, List<String> exclusions, IParser parser, boolean hash,int threadCount,int pagesPerThread) {
        Assert.notNull(parser,"Parser must not be null");
        this.site = site;
        this.exclusions = exclusions;
        this.parser = parser;
        siteKey = StringUtils.getDocumentType(site);
        this.hash = hash;
        this.threadCount=threadCount;
        this.pagesPerThread=pagesPerThread;

    }

    public URL getSite() {
        return site;
    }

    public List<String> getExclusions() {
        return exclusions;
    }

    public IParser getParser() {
        return parser;
    }

    public String getSiteKey() {
        return siteKey;
    }

    public boolean isHash() {
        return hash;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public int getPagesPerThread() {
        return pagesPerThread;
    }
}

