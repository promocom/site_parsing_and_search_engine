package my.cen.model.spider;

import groovy.lang.GroovyClassLoader;
import my.cen.parser.IParser;
import my.cen.utils.DocumentUtils;
import my.cen.utils.StringUtils;
import org.elasticsearch.common.collect.ImmutableCollection;
import org.elasticsearch.common.collect.ImmutableList;
import org.elasticsearch.common.collect.ImmutableMap;
import org.elasticsearch.common.collect.ImmutableSet;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Created by PC on 15.12.2014.
 */
public class SiteParsingOptionsHolder implements ISiteParsingOptionsHolder {
    private final String template;
    private final String storePath;
    private final int threads;
    private final int pages;
    private final Map<String, SiteParsingOptions> optionsMap = new HashMap<>();
    private final List<SiteParsingOptions> importedOptions = new ArrayList<>();

    public SiteParsingOptionsHolder(String template, String storePath, int threads, int pages) {
        this.template = template;
        this.threads = threads;
        this.pages = pages;
        this.storePath = storePath;
    }

    public SiteParsingOptions load(File file) {

        try {

            GroovyClassLoader gcl = new GroovyClassLoader();
            Class clazz = gcl.parseClass(file);
            IParser parser = (IParser) clazz.newInstance();
            Map<String, Object> vars = new HashMap<>(ImmutableMap.of("init", true));
            parser.parse(vars);
            String site = String.valueOf(vars.get("site"));
            boolean hash = vars.get("hash") != null && (boolean) vars.get("hash");
            List<String> excludeParts = Arrays.asList(String.valueOf(vars.get("skipParts")).split(","));
            SiteParsingOptions option = new SiteParsingOptions(new URL(site), excludeParts, parser, hash, threads, pages);

            optionsMap.put(option.getSiteKey(), option);
            return option;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return null;

    }

    public SiteParsingOptions get(String site) {
        return optionsMap.get(site);
    }

    public Set<String> allSites() {
        return ImmutableSet.copyOf(optionsMap.keySet());
    }

    public Collection<SiteParsingOptions> allOptions() {
        return ImmutableList.copyOf(optionsMap.values());
    }

    @Override
    public Collection<SiteParsingOptions> importedOptions() {
        Collection<SiteParsingOptions> options = ImmutableList.copyOf(importedOptions);
        importedOptions.clear();
        return options;
    }

    public int size() {
        return optionsMap.size();
    }

    public void addFromTemplate(String domain) {
        String name = StringUtils.encode(domain);
        String content = template.replace("_$HOST$_", DocumentUtils.HTTP_PREFIX + domain).replace("_$NAME$_", name);
        File groovyScript = new File(storePath + domain + ".groovy");
        if (!groovyScript.exists()) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(groovyScript))) {
                writer.write(content);
            } catch (IOException e) {
                e.printStackTrace();
            }
            load(groovyScript);
        }
    }
}
